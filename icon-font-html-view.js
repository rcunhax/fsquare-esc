/**
 * Created by ruic on 14/03/2016.
 */

var path = require('path'),
    gutil = require('gulp-util'),
    consolidate = require('consolidate'),
    _ = require('lodash'),
    Stream = require('stream');

var PLUGIN_NAME  = 'gulp-iconfont-view-html';

function iconFontViewHtml(config) {
    var glyphMap = [],
        stream,
        outputFile,

    config = _.merge({
        templatePath: 'icons-display-template.html',
        targetPath: '_icons-display-template.html',
        fontCssFilePath: './icons.css',
        engine: 'lodash',
        cssClass: 'icon'
    }, config);

    stream = Stream.PassThrough({
        objectMode: true
    });

    stream._transform = function(file, unused, cb) {

        if (file.isNull()) {
            this.push(file);
            return cb();
        }

        // Create output file
        if (!outputFile) {
            outputFile = new gutil.File({
                base: file.base,
                cwd: file.cwd,
                path: path.join(file.base, config.targetPath),
                contents: file.isBuffer() ? new Buffer(0) : new Stream.PassThrough()
            });

        }

        glyphMap.push({
            classPrefix: config.cssClass,
            className: config.cssClass+"-"+path.basename(file.path, '.svg'),
            fileName: path.basename(file.path, '.svg'),
        });

        cb();
    }

    stream._flush = function(cb) {
        var content;
        if (glyphMap.length) {
            consolidate[config.engine](config.templatePath, {
                glyphs: glyphMap,
                fontCssFilePath: config.fontCssFilePath,
                buildDate: config.buildDate,
                iconsTitle: config.iconsTitle,
                pageTitle: config.pageTitle
            }, function(err, html) {
                if (err) {
                    console.log("err: "+err);
                    throw new gutil.PluginError(PLUGIN_NAME, 'Error in template: ' + err.message);
                }

                content = Buffer(html);

                if (outputFile.isBuffer()) {
                    outputFile.contents = content;
                } else {
                    outputFile.contents.write(content);
                    outputFile.contents.end();
                }

                stream.push(outputFile);

                cb();

            });
        } else {
            cb();
        }

    }

    return stream;
}

module.exports = iconFontViewHtml;