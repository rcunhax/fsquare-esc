/**
 * Created by ruic on 03/04/2016.
 */

var logger = require('./loggerConfig');
var uuid = require('uuid');
var express = require('express');
var session = require('express-session');
var ejs = require('ejs');



var app = express();
var config = require('./config');


module.exports = function(rootDir) {

    var apiController = require('./controllers/httpController');

    var port = process.env.PORT || 3005;

    app.set('views', rootDir+'/app');
    app.use('/', express.static(rootDir+'/client/'));
    //app.use('/', express.static(rootDir+'/dist/'));

    
    app.set('view engine', 'html');
    app.engine('html', ejs.renderFile);

    app.use('/', function (req, res, next) {
        next();
    });
    
    apiController(app);

    app.listen(port);

    console.log(rootDir+"--Started--"+__dirname);


}
