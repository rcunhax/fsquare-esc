var fs = require('fs');
var Client = require('node-rest-client').Client;
var configValues = require('../config/config');
var _ = require('lodash');
var NodeCache = require( "node-cache" );

module.exports = {
	client: new Client(),
	CACHE: new NodeCache(),

	clientCall: function (url, args, callback){
		console.log('URL: '+url);
		console.log('args: '+JSON.stringify(args));
		this.client.get(url, args, function (data, response) {
			//console.log("response: "+response);
			if(callback){
				callback(data, response);
			}

		}).on('error', function (err) {
			console.log('something went wrong on the request', err.request.options);
		}).on('responseTimeout', function (res) {
			console.log("response has expired");
		});
	},


	setArgs: function (parameters){
		var args = {
			path: { "version": 'v1' },
			parameters: {
				apiKey: configValues["adultwork-api-key"]
			},
			headers: {
				"Content-Type": "application/json",
				"X-CaseType": "camelcase"
			}
		};
		console.log('-------------------------');
		console.log('1 args.parameters: '+JSON.stringify(args.parameters));
		_.assign(args.parameters, parameters);
		console.log('2 args.parameters: '+JSON.stringify(args.parameters));
		return args;

	},

	verifyCredentials: function(callback){
		var args = this.setArgs({});
		this.clientCall(this.getURL("/account/verifyCredentials"), args, callback);

	},

	getProfile: function(userId, callback){
		var args = this.setArgs({
			UserID: userId,
			ReturnProfile: true,
			ReturnDetails: true,
			ReturnImages: true,
			ReturnContact: true,
			ReturnFAQs: true,
			ReturnInterview: true,
			ReturnLinks: true,
			ReturnVitalStats: true,
			ReturnNationality: true,
			ReturnFeatured: true,
			ReturnLoggedInNow: true,
			ReturnAlternativePractices: true,
			ReturnOtherServices: true,
			ReturnEscortRates: true
		});
		this.clientCall(this.getURL("/profile/getProfileDetails"), args, callback);
	},


	getMovies: function(params, pageNumber, callback){
		var args = this.setArgs({
			PageNumber: pageNumber
		});
		this.clientCall(this.getURL("/search/searchMovies"), args, callback);
	},

	getProfileMovies: function(params, pageNumber, callback){
		var args = this.setArgs({
			PageNumber: pageNumber
		});
		this.clientCall(this.getURL("/search/searchMovies"), args, callback);
	},

	getProfileGallery: function(userId, pageNumber, callback){
		var args = this.setArgs({
			UserID: userId,
			PageNumber: pageNumber
		});
		this.clientCall(this.getURL("/pictures/getFreeGallery"), args, callback);
	},
	getProfilePrivateGallery: function(userId, pageNumber, callback){
		var args = this.setArgs({
			UserID: userId,
			PageNumber: pageNumber
		});
		this.clientCall(this.getURL("/pictures/getGallery"), args, callback);
	},

	getProfileRatings: function(userId, pageNumber, callback){
		var args = this.setArgs({
			UserID: userId,
			ReturnOverview: true,
			PageNumber: pageNumber,
			ReturnFeedback: true,
			ReturnEscortFeedback:true
		});
		this.clientCall(this.getURL("/profile/getRatings"), args, callback);
	},

	getProfiles: function(countryId, pageNumber, params, callback){
		var args = this.setArgs(params);
		this.clientCall(this.getURL("/search/searchProfiles"), args, callback);
	},

	searchFieldReports: function(params, callback){
		var args = this.setArgs(params);
		this.clientCall(this.getURL("/search/searchFieldReports"), args, callback);
	},

	getProfileFieldReports: function(params, callback){
		var args = this.setArgs(params);
		this.clientCall(this.getURL("/search/searchFieldReports"), args, callback);
	},


	getCreditsHistory: function(callback){
		var args = this.setArgs({});
		this.clientCall(this.getURL("/credithistory/GetCreditsHistory"), args, callback);
	},

	getProfileBlog: function(userId, pageNumber, callback){
		var args = this.setArgs({
			UserID: userId,
			PageNumber: pageNumber
		});
		this.clientCall(this.getURL("/blogs/getUserBlogs"), args, callback);

	},

	getBlog: function(blogId, callback){
		var args = this.setArgs({
			BlogID: blogId
		});
		this.clientCall(this.getURL("/blogs/getBlog"), args, callback);
	},

	getTopMovies: function(params, callback){
		var args = this.setArgs(params);
		this.clientCall(this.getURL("/lists/getTopList"), args, callback);
	},

	getTopEscorts: function(params, callback){
		var args = this.setArgs(params);
		this.clientCall(this.getURL("/lists/getTopList"), args, callback);
	},



	getContentGenders: function(callback){
		var args = this.setArgs({});
		this.clientCall(this.getURL("/lists/getContentGenders"), args, callback);
	},

	getMovieFormats: function(callback){
		var args = this.setArgs({});
		this.clientCall(this.getURL("/lists/getMovieFormats"), args, callback);
	},

	getCountries: function(params, callback){

		//this.CACHE.set( key, val, [ ttl ], [callback] )

		var args = this.setArgs(params);
		this.clientCall(this.getURL("/lists/getCountries"), args, callback);
	},

	getOrientations: function(callback){
		var args = this.setArgs({});
		this.clientCall(this.getURL("/lists/getOrientations"), args, callback);
	},

	getGenders: function(callback){
		var args = this.setArgs({
			HideGay: false,
			HideStraight: false,
			HideCouples: false
		});
		this.clientCall(this.getURL("/lists/getGenders"), args, callback);
	},

	getRegions: function(countryId, callback){
		var args = this.setArgs({CountryID: countryId});
		this.clientCall(this.getURL("/lists/getRegions"), args, callback);
	},

	getCounties: function(regionId, callback){
		var args = this.setArgs({RegionID: regionId});
		this.clientCall(this.getURL("/lists/getCounties"), args, callback);
	},

	getEnjoys: function(countryId, callback){
		var args = this.setArgs({CountryID: countryId});
		this.clientCall(this.getURL("/lists/getEnjoys"), args, callback);
	},

	getAlternativePractices: function(callback){
		this.clientCall(this.getURL("/lists/getAlternativePractices"), this.setArgs({}), callback);
	},

	getEthnicity: function(callback){
		this.clientCall(this.getURL("/lists/getEthnicity"), this.setArgs({}), callback);
	},
	// get: function(countryId, callback){
	// 	var args = this.setArgs({CountryID: countryId});
	// 	this.clientCall(this.getURL("/lists/"), args, callback);
	// },


	getURL: function(path){
		return configValues['adultwork_sandbox_base_url']+"${version}" + path;
	}

}
