/**
 * Created by ruic on 17/08/2016.
 */
var bodyParser = require('body-parser');
var fs = require('fs');
var configValues = require('../config/config');

var adwAPI = require('./adwAPI');

module.exports = function(app) {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.get('/api/verify-credentials', function(req, res) {
        adwAPI.verifyCredentials(function(data){res.send(data)});
    });

    app.get('/api/profile/:userId', function(req, res) {
        adwAPI.getProfile(req.params.userId, function(data){res.send(data)});
    });

    app.get('/api/profile/blog/:userId/:pageNumber', function(req, res) {
        adwAPI.getProfileBlog(req.params.userId, req.params.pageNumber, function(data){res.send(data)});
    });

    app.get('/api/blog/:blogId', function(req, res) {
        adwAPI.getBlog(req.params.blogId, function(data){res.send(data)});
    });

    app.get('/api/profile/gallery/:userId/:pageNumber', function(req, res) {
        adwAPI.getProfileGallery(req.params.userId, req.params.pageNumber, function(data){res.send(data)});
    });

    app.get('/api/top-movies', function(req, res) {
        var parameters = {
            Type:'MovieViews',
            DateRange: 'LastMonth',
        };
        if(req.query.numberOfRecords){
            parameters['NumberOfRecords'] = req.query.numberOfRecords;
        }
        if(req.query.dateRange){
            parameters['DateRange'] = req.query.dateRange;
        }

        adwAPI.getTopMovies(parameters, function(data){res.send(data)});
    });

    app.get('/api/top-escorts', function(req, res) {
        var parameters = {
            Type:'EscortRatings',
            DateRange: 'LastMonth',
        };
        if(req.query.numberOfRecords){
            parameters['NumberOfRecords'] = req.query.numberOfRecords;
        }
        if(req.query.dateRange){
            parameters['DateRange'] = req.query.dateRange;
        }

        adwAPI.getTopEscorts(parameters, function(data){res.send(data)});
    });

    app.get('/api/profile/private-gallery/:userId/:pageNumber', function(req, res) {
        adwAPI.getProfilePrivateGallery(req.params.userId, req.params.pageNumber, function(data){res.send(data)});
    });

    app.get('/api/profile/ratings/:userId/:pageNumber', function(req, res) {
        adwAPI.getProfileRatings(req.params.userId, req.params.pageNumber, function(data){res.send(data)});
    });

    app.get('/api/field-reports/:countryId/:pageNumber', function(req, res) {
        var parameters = {
            UserID: req.params.userId,
            PageNumber: req.params.pageNumber,
            PageSize: 5,
            OrderBy: 'createdate',
        };
        if(req.query.orderBy){
            parameters['OrderBy'] = req.query.orderBy;
        }
        adwAPI.searchFieldReports(parameters, function(data){res.send(data)});
    });


    app.get('/api/profile/field-report/:userId/:pageNumber', function(req, res) {
        var parameters = {
            UserID: req.params.userId,
            PageNumber: req.params.pageNumber,
            ProfilesPerPage: 5,
            OrderBy: 'createdate',
        };
        if(req.query.orderBy){
            parameters['OrderBy'] = req.query.orderBy;
        }
        if(req.query.regionId){
            parameters['RegionID'] = req.query.regionId;
        }
        if(req.query.countyId){
            parameters['CountyID'] = req.query.countyId;
        }

        if(req.query.inCall){
            parameters['InCall'] = req.query.inCall;
        }

        if(req.query.outCall){
            parameters['OutCall'] = req.query.outCall;
        }
        adwAPI.getProfileFieldReports(parameters, function(data){res.send(data)});
    });

    app.get('/api/profile/movies/:userId/:pageNumber', function(req, res) {
        // var parameters = {
        // 	CountryID: req.params.userId,
        // 	PageNumber: req.params.pageNumber,
        // 	ProfilesPerPage: 5,
        // 	OrderBy: 'lastupdated',
        // 	IsEscort: true
        // };

        adwAPI.getProfileMovies(req.params.userId, req.params.pageNumber, function(data){res.send(data)});
    });


    app.get('/api/movies/:pageNumber', function(req, res) {
        var parameters = {
            PageNumber: req.params.pageNumber,
        };

        adwAPI.getMovies(req.params.pageNumber, function(data){res.send(data)});
    });

    app.get('/xxx', function(req, res) {});


    app.get('/api/profiles/:countryId/:pageNumber', function(req, res) {

        var parameters = {
            CountryID: req.params.countryId,
            PageNumber: req.params.pageNumber,
            ProfilesPerPage: 5,
            OrderBy: 'lastupdated',
            IsEscort: true
        };

        if(req.query.profilesPerPage){
            parameters['ProfilesPerPage'] = req.query.profilesPerPage;
        }
        if(req.query.orderBy){
            parameters['OrderBy'] = req.query.orderBy;
        }

        if(req.query.genderIds){
            parameters['GenderIDs'] = req.query.genderIds;
        }
        if(req.query.orientationIds){
            parameters['OrientationIds'] = req.query.orientationIds;
        }

        if(req.query.hasMovieClips){
            parameters['HasMovieClips'] = req.query.hasMovieClips;
        }

        if(req.query.orientationIds){
            parameters['OrientationIds'] = req.query.orientationIds;
        }
        if(req.query.locationZipCode){
            parameters['LocationZipCode'] = req.query.locationZipCode;
        }
        if(req.query.locationProximityMiles){
            parameters['LocationProximityMiles'] = req.query.locationProximityMiles;
        }
        if(req.query.locationProximityKilometres){
            parameters['locationProximityKilometres'] = req.query.locationProximityKilometres;
        }

        if(req.query.isVerified){
            parameters['IsVerified'] = req.query.isVerified == 'true';
        }
        if(req.query.isAvailableTodayEscort){
            parameters['IsAvailableTodayEscort'] = req.query.isAvailableTodayEscort;
        }

        if(req.query.hasGallery){
            parameters['HasGallery'] = req.query.hasGallery;
        }

        if(req.query.isAlternative){
            parameters['IsAlternative'] = req.query.isAlternative;
        }

        adwAPI.getProfiles(req.params.countryId, req.params.pageNumber, parameters, function(data){res.send(data)});

    });

    app.get('/api/countries', function(req, res) {
        var parameters= {};
        if(req.params.excludeCountryIDs){
            parameters['ExcludeCountryIDs'] = req.params.excludeCountryIDs;
        }
        adwAPI.getCountries(parameters, function(data){res.send(data)});
    });

    app.get('/api/credits-history', function(req, res) {
        adwAPI.getCreditsHistory(function(data){res.send(data)});
    });



    app.get('/api/orientations', function(req, res) {
        adwAPI.getOrientations(function(data){res.send(data)});
    });


    app.get('/api/content-genders', function(req, res) {
        adwAPI.getContentGenders(function(data){res.send(data)});
    });

    app.get('/api/movie-formats', function(req, res) {
        adwAPI.getMovieFormats(function(data){res.send(data)});
    });

    app.get('/api/genders', function(req, res) {
        adwAPI.getGenders(function(data){res.send(data)});
    });

    app.get('/api/regions/:countryId', function(req, res) {
        adwAPI.getRegions(req.params.countryId, function(data){res.send(data)});
    });

    app.get('/api/counties/:regionId', function(req, res) {
        adwAPI.getCounties(req.params.regionId, function(data){res.send(data)});
    });

    app.get('/api/enjoys/:countryId', function(req, res) {
        adwAPI.getEnjoys(req.params.countryId, function(data){res.send(data)});
    });

    app.get('/api/alternative-practices', function(req, res) {
        adwAPI.getAlternativePractices(function(data){res.send(data)});
    });

    app.get('/api/ethnicities', function(req, res) {
        adwAPI.getEthnicity(function(data){res.send(data)});
    });

    app.get('/oauth/access_token', function(req, res) {
        console.log('/oauth/access_token: '+ JSON.stringify(req.query));
        res.redirect('/');
    });

    app.get('/oauth/authorize', function(req, res) {
        console.log('/oauth/authorize: '+ JSON.stringify(req.query));
        res.redirect('/');
    });

    app.get('/api/logout', function(req, res) {

        res.redirect('/');

    });

    app.post('/api/login', function(req, res) {

    });

}
