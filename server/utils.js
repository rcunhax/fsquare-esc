/**
 * Created by ruic on 17/05/2016.
 */

var fs = require('fs');
module.exports = {

    FILE_FOLDER: 'uploads/',

    deleteFiles: function(files){
        files.forEach(function(filename) {
            if(filename != this.FILE_FOLDER+'default.jpg')
                fs.unlink(filename);
        });
    },

    getFile: function(fileId, callback){
        if(fileId){
            var match = fileId.match(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g);
            if(!match){
                fileId ='default.jpg';
            }

        }else{
            fileId ='default.jpg';
        }
        console.log('this.FILE_FOLDER: '+this.FILE_FOLDER);
        var filePath = this.FILE_FOLDER+fileId;
        console.log('filename: '+filePath);
        var file = fs.readFileSync(filePath);
        callback(file);
    }

}