var conf = require('./conf.json');

module.exports = function(config) {

    /**
     * This is for allowing to pass which browsers test as parameters (e.g: ...browsers-all, ...browsers-chrome-firefox)
     * if none if passed phantomJS will be selected
     *
     *
     */
    var browsers = [];
    for(var i= 0; i < process.argv.length; i++){
        var name = process.argv[i];
        if(name.startsWith('browsers')){
            var brws = name.split('-');

            if(brws[1] == 'all'.toLowerCase()){
                for(key in conf.existingBrowsers){
                    browsers.push(conf.existingBrowsers[key]);
                }
            }else{
                for(var j = 1; j < brws.length; j++){
                    var browser = conf.existingBrowsers[brws[j].toLowerCase()];
                    if(browser){
                        browsers.push(browser);
                    }else{
                        console.warn("Browser named '"+brws[j]+"' does not exist");
                    }
                }
            }
        }
    }

    if(browsers.length == 0){
        browsers.push(conf.existingBrowsers['phantomjs']);
    }

    //*****************************************
    
    config.set({

        basePath: '',

        frameworks: ['jasmine'],
        distFolder: conf.distFolder,

        files: [
            // Polyfills.
            'node_modules/es6-shim/es6-shim.js',
            // 'node_modules/es6-shim/es6-shim.js.map',

            'node_modules/reflect-metadata/Reflect.js',
            // 'node_modules/reflect-metadata/Reflect.js.map',

            // System.js for module loading
            'node_modules/systemjs/dist/system-polyfills.js',
            // 'node_modules/systemjs/dist/system-polyfills.js.map',
            'node_modules/systemjs/dist/system.src.js',
            // 'node_modules/systemjs/dist/system.src.js.map',

            // Zone.js dependencies
            'node_modules/zone.js/dist/zone.js',
            'node_modules/zone.js/dist/jasmine-patch.js',
            'node_modules/zone.js/dist/async-test.js',
            'node_modules/zone.js/dist/fake-async-test.js',

            // RxJs.
            { pattern: 'node_modules/rxjs/**/*.js', included: false, watched: false },
            { pattern: 'node_modules/rxjs/**/*.js.map', included: false, watched: false },


            {pattern: 'karma-test-shim.js', included: true, watched: true},
            //{pattern: 'built/test/matchers.js', included: true, watched: true},

            // paths loaded via module imports
            // Angular itself
            {pattern: 'node_modules/@angular/**/*.js', included: false, watched: true},
            {pattern: 'node_modules/@angular/**/*.js.map', included: false, watched: true},

            // Our built application code
            {pattern: conf.distFolder+'/app/**/*.js', included: false, watched: true},
            //{pattern: conf.distFolder+'/app/**/*.js.map', included: false, watched: true},

            // paths loaded via Angular's component compiler
            // (these paths need to be rewritten, see proxies section)
            {pattern: conf.distFolder+'/**/*.html', included: false, watched: true},
            {pattern: conf.distFolder+'/**/*.css', included: false, watched: true},

            // paths to support debugging with source maps in dev tools
            {pattern: 'src/app/**/*.ts', included: false, watched: false},
            {pattern: 'src/app/**/*.js.map', included: false, watched: false}
        ],

        // proxied base paths
        proxies: {
            // required for component assests fetched by Angular's compiler
            "/app/": "/base/"+conf.distFolder+"/app/",
            "/src/": "/base/src/"
        },

        reporters: ['progress', 'html'],

        // HtmlReporter configuration
        htmlReporter: {
            // Open this file to see results in browser
            //outputFile: '_test-output/tests_'+date.toString()+'.html',
            outputFile: '_test-output/tests.html',
            // Optional
            pageTitle: 'Unit Tests',
            subPageTitle: __dirname
        },

        port: 9877,
        colors: true,
        logLevel: config.LOG_DEBUG,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false
    })
}