var gulp = require('gulp');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var browserSync = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var gulpSequence = require('gulp-sequence');
var template = require('gulp-template');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var del = require('del');

var connect = require('gulp-connect');
var $ = require('gulp-load-plugins')();
var iconfont = require('gulp-iconfont');
var url = require('url');
var proxy = require('proxy-middleware');
var iconfontCss = require('gulp-iconfont-css');
var gulpTypescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');
var filter = require("stream-filter");
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var tslint = require('gulp-tslint');
var iconFontHtmlView = require('./icon-font-html-view.js');


var conf = require('./conf.json');



gulp.task('icon-font-html-view',function(){
    return gulp.src(['src/images/icons/**/*.svg'])
        .pipe(iconFontHtmlView({
            templatePath: 'icons-display-template.html',
            targetPath: 'fsquare-icons.html',
            cssClass: 'fs',
            fontCssFilePath: '/styles/fsquare-icons.css',
            buildDate: new Date(),
            iconsTitle: 'Fsquare Icons',
            pageTitle: 'Fsquare Icons'
        }))
        .pipe(gulp.dest(conf.distFolder+'/styles/fonts/fsquare/view'))
        .pipe(gulp.dest('.tmp/styles/fonts/fsquare/view'));

});

gulp.task('icon-font', function(){
    return gulp.src(['src/images/icons/**/*.svg'])
        .pipe(iconfontCss({
            fontName: 'fsquareIcons',
            path: 'icons-css-file-template.css',
            targetPath: '../../fsquare-icons.css',
            fontPath: './fonts/fsquare/',
            cssClass: 'fs'
        }))
        .pipe(iconfont({
            fontName: 'fsquareIcons',
            normalize: true,
            formats: ['ttf', 'eot', 'woff', 'woff2']
        }))
        .pipe(gulp.dest(conf.distFolder+'/styles/fonts/fsquare'))
        .pipe(gulp.dest('.tmp/styles/fonts/fsquare'));
});

gulp.task('build-icons', function (cb) {
    gulpSequence('icon-font','icon-font-html-view',
        cb);
})




gulp.task('styles', function (cb) {
    gulpSequence('sass','fonts','images',
        cb);
});

gulp.task('sass',function() {
    return gulp.src(['./src/styles/style.scss'])

        .pipe(plumber(function(err){
            console.log("*** Style Error ***");
            console.log(err);
        }))
        .pipe(sass())
        .pipe(csso())
        .pipe(plumber.stop())
        .pipe(autoprefixer({browsers: ['last 1 version']}))
        .pipe(gulp.dest('.tmp/styles'))

        .pipe(gulp.dest(conf.distFolder+'/styles'))
        .pipe(browserSync.reload({stream: true}));
});


gulp.task('fonts',function() {
    return gulp.src(['./node_modules/**/*.{eot,svg,ttf,woff,woff2}', 'src/styles/**/*.{eot,svg,ttf,woff,woff2}'])
        .pipe($.flatten())
        .pipe(gulp.dest(conf.distFolder+'/styles/fonts'))
        .pipe(gulp.dest('.tmp/styles/fonts'));
});

gulp.task('images', function() {
    return gulp.src('src/images/**/*')
        .pipe(gulp.dest('.tmp/images'))
        .pipe(gulp.dest(conf.distFolder+'/images'));
});

gulp.task('tslint', function() {
    return gulp.src("src/**/*.ts")
        .pipe(tslint())
        .pipe(tslint.report('prose'));
});

gulp.task('typescript-parse',function() {
    //TODO: change to use project's 'tsconfig.json'

    return gulp.src(['src/**/*.ts','!src/**/*.spec.ts'])
        .pipe(sourcemaps.init())
        .pipe(gulpTypescript({
            target: "es5",
            module: "system",
            moduleResolution: "node",
            emitDecoratorMetadata: true,
            experimentalDecorators: true,
            removeComments: false,
            noImplicitAny: false
        }))

        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp/'))
        //TODO: Should remove 'dist' from the destination?
        // .pipe(filter(function(data) {
        //     return data.history[0].endsWith(".js");//Filter to only accept JS files
        // }))
        // .pipe(uglify())
        .pipe(gulp.dest(conf.distFolder));

});

gulp.task('component-template-parse', function() {
    return gulp.src('src/**/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            caseSensitive: true
        }))
        .pipe(gulp.dest(conf.distFolder));
});


gulp.task('components', function (cb) {
    gulpSequence('typescript-parse',
        'component-template-parse',
        'styles',
        cb);
});

gulp.task('lib', function() {

    var arrDev = [
        './node_modules/es6-shim/es6-shim.min.js',
        './node_modules/systemjs/dist/system-polyfills.js',
        './node_modules/systemjs/dist/system.src.js',
        './node_modules/rxjs*/**/*.js',
        './node_modules/@angular*/**/*.js',
        './node_modules/reflect-metadata/Reflect.js',
        './node_modules/zone.js/dist/zone.js',
        './node_modules/ie-shim*/index.js',
        './node_modules/core-js*/client/shim.js',
        './node_modules/angular2-in-memory-web-api*/index.js',
        './node_modules/moment*/min/moment-with-locales.min.js',

        './node_modules/es6-shim/es6-shim.map',
        './node_modules/systemjs/dist/system-polyfills.map',
        './node_modules/systemjs/dist/system.src.map',
        './node_modules/rxjs*/**/*.map',
        './node_modules/@angular*/**/*.map',
        './node_modules/reflect-metadata/Reflect.js.map',
        './node_modules/zone.js/dist/zone.min.map'
    ];

    gulp.src(arrDev)
        .pipe(gulp.dest('.tmp/lib/'));

    gulp.src(arrDev)
        .pipe(gulp.dest(conf.distFolder+'/lib'))

});

var proxyOptionsApi = url.parse('http://localhost:3005/api');
proxyOptionsApi.route = '/api';

gulp.task('serve', function() {

    browserSync.init({
        server:{
            port: 3002,
            baseDir: ["./src","./.tmp/"],
            middleware: [
                proxy(proxyOptionsApi)
            ]
        }
    });
    gulp.watch('src/**/*.scss',['sass']);
    gulp.watch('src/**/*.ts').on('change', browserSync.reload);
    gulp.watch('src/**/*.html').on('change', browserSync.reload);
});

gulp.task('tests-serve', function() {
    browserSync.init({
        server:{
            port: 3004,
            baseDir: ["./src","./node_modules/","./.tmp/"],
            middleware: []
        }
    });
    gulp.watch('src/**/*test.ts').on('change', browserSync.reload);
});

gulp.task('default', function (cb) {
    gulpSequence('build', 'build-icons', 'serve', cb);
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('systemjs-conf', function() {
    return gulp.src('./src/systemjs.conf.js')
        .pipe(gulp.dest('.tmp/'))
        .pipe(gulp.dest(conf.distFolder))

});

gulp.task('server', function() {
    return gulp.src(['./server*/**','./index.js','./package.json'])
        .pipe(gulp.dest(conf.serverDistFolder))

});

gulp.task('build-tests', function() {
    return gulp.src('src/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(gulpTypescript({
            target: "es5",
            module: "system",
            moduleResolution: "node",
            emitDecoratorMetadata: true,
            experimentalDecorators: true,
            removeComments: false,
            noImplicitAny: false,
            suppressImplicitAnyIndexErrors: true

        })).pipe(gulp.dest('./src/'))
});

gulp.task('build', function (cb) {
    gulpSequence('clean','lib', 'components', 'server',
        'systemjs-conf',
        cb);
});
