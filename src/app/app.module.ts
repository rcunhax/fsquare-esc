/**
 * Created by ruic on 29/08/2016.
 */


import { NgModule } from '@angular/core'
import { FormsModule } from "@angular/forms";
import { BrowserModule, Title } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppComponent } from "./components/app.component";
import { routing } from "./components/app.routes";
import { SearchService } from "./services/search.service";
import { SearchModule } from "./components/search/search.component";
import { GalleryModule} from "./components/gallery/gallery.component";
import { ProfileModule} from "./components/profile/profile.component";
import { ImageSliderModule} from "./components/image-slider/image-slider.component";
import { FilterComponent } from "./components/filter/filter.component";
import { HomeComponent } from "./components/home/home.component";
import { ApplicationService } from "./services/application.service";
import { MovieItemModule } from "./components/movie-item/movie-item.component";
import { EscortItemModule} from "./components/escort-item/escort-item.component";
import {PipesModule} from "./pipes/pipes.module";

@NgModule({
    declarations: [
        AppComponent,
        FilterComponent,
        HomeComponent
    ],
    imports     : [
        PipesModule,
        GalleryModule,
        SearchModule,
        ImageSliderModule,
        ProfileModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        routing,
        MovieItemModule,
        EscortItemModule
    ],
    providers   : [
        SearchService,
        ApplicationService,
        Title,
        RouterModule,
        {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
    bootstrap   : [AppComponent]
})

export class AppModule {}
