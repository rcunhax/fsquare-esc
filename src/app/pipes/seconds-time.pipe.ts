/**
 * Created by ruic on 19/09/2016.
 */


import { Pipe, PipeTransform, NgModule } from '@angular/core';


@Pipe({
    name: 'fsSecondsTime'
})
export class SecondsTimePipe implements PipeTransform {
    transform(value:number): string {
        let hours:number    = Math.floor(value / 3600);
        let minutes:number  = Math.floor((value - (hours * 3600)) / 60);
        let seconds:number  = value - (hours * 3600) - (minutes * 60);


        let hoursDisplay:string;
        let minutesDisplay:string;
        let secondsDisplay:string;
        if (hours < 10) {
            hoursDisplay   = "0"+hours;
        }
        if (minutes < 10) {
            minutesDisplay = "0"+minutes;
        }
        if (seconds < 10) {
            secondsDisplay = "0"+seconds;
        }

        return hoursDisplay+':'+minutesDisplay+':'+secondsDisplay;
    }
}