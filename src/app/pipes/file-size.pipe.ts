/**
 * Created by ruic on 19/09/2016.
 */

import { Pipe, PipeTransform, NgModule } from '@angular/core';


@Pipe({
    name: 'fsFileSize'
})
export class FileSizePipe implements PipeTransform {
    transform(value:number, decimals?:number): string {
        if(value == 0)
            return '0 Byte';
        
        let k = 1000;
        let dm = decimals + 1 || 3;
        let sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        let i = Math.floor(Math.log(value) / Math.log(k));
        let size =  parseFloat((value / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];

        return size;
    }
}