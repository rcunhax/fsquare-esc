/**
 * Created by ruic on 19/09/2016.
 */

import { NgModule } from '@angular/core';
import { LocationPipe } from "./location.pipe";
import { SecondsTimePipe } from "./seconds-time.pipe";
import { FileSizePipe } from "./file-size.pipe";

@NgModule({
    declarations: [
        LocationPipe,
        SecondsTimePipe,
        FileSizePipe
    ],
    exports: [
        LocationPipe,
        SecondsTimePipe,
        FileSizePipe

    ]
})

export class PipesModule {}