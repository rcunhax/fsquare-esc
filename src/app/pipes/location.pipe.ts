/**
 * Created by ruic on 15/08/2016.
 */


import { Pipe, PipeTransform, NgModule } from '@angular/core';


@Pipe({
        name: 'fsLocation'
})
export class LocationPipe implements PipeTransform {
    transform(value:any, separator): string {
        separator = separator || ', ';
        let location:string;

        if(value.town){
            if(location){
                location = `${location}${separator}${value.town}`;
            }else{
                location = `${value.town}`;
            }

        }

        if(value.locationZipCode){
            if(location){
                location = `${location}${separator}${value.locationZipCode}`;
            }else{
                location = `${value.locationZipCode}`;
            }
        }



        if(value.county){
            if(location){
                location = `${location}${separator}${value.county}`;
            }else{
                location = `${value.county}`;
            }
        }

        if(value.region){
            if(location){
                location = `${location}${separator}${value.region}`;
            }else{
                location = `${value.region}`;
            }
        }

        return location;
    }
}

