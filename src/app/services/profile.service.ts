/**
 * Created by ruic on 07/08/2016.
 */


import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';

import {UserModel, BlogModel, MovieModel} from "../models/models";
import "rxjs/add/operator/take";
import "rxjs/add/operator/cache";

const PROFILE_URL = '/api/profile';
const RATINGS_URL = '/api/profile/ratings';
const GALLERY_URL = '/api/profile/gallery';
const PRIVATE_GALLERY_URL = '/api/profile/private-gallery';
const PROFILE_BLOG_URL = '/api/profile/blog';
const FIELD_REPORT_URL = '/api/profile/field-report';
const BLOG_URL = '/api/blog';
const MOVIE_CLIPS_URL = '/api/profile/movies';

@Injectable()
export class ProfileService {


    menuPermissionsEvent:EventEmitter<Array<any>> = new EventEmitter<Array<any>>();
    user:UserModel;

    userSetEvent:EventEmitter<UserModel> = new EventEmitter<UserModel>();

    constructor(private http:Http) {
    }

    getRatings(userId:number, pageNr:number):Observable<JSON>{
        return this.http.get(`${RATINGS_URL}/${userId}/${pageNr}`).map(res => res.json());
    }
	getGallery(userId:number, pageNr:number):Observable<JSON>{
        return this.http.get(`${GALLERY_URL}/${userId}/${pageNr}`).map(res => res.json());
    }

    getPrivateGallery(userId:number, pageNr:number):Observable<JSON>{
        return this.http.get(`${PRIVATE_GALLERY_URL}/${userId}/${pageNr}`).map(res => res.json());
    }

    getFieldReport(userId:number, pageNr:number):Observable<JSON>{
        return this.http.get(`${FIELD_REPORT_URL}/${userId}/${pageNr}`).map(res => res.json());
    }

    getMovieClips(userId:number, pageNr:number):Observable<any>{
        return this.http.get(`${MOVIE_CLIPS_URL}/${userId}/${pageNr}`).map(res => res.json());
    }

    getProfile(userId:number):Observable<UserModel>{
        if(this.user && this.user.profile.userID == userId){
            return Observable.of(this.user);
        }else{
            this.user = null;

            var subscriber:Subscriber<UserModel>;
            var observable:Observable<UserModel> = new Observable<UserModel>((_subscriber:Subscriber<UserModel>) => {
                subscriber = _subscriber;
            });

            this.http.get(`${PROFILE_URL}/${userId}`).map(res => res.json()).subscribe((user:UserModel) => {
                subscriber.next(user);
                this.user = user;
                this.userSetEvent.emit(user);
            });

            return observable;
        }
    }

    getBlogs(userId:number, pageNr:number):Observable<any>{
        return this.http.get(`${PROFILE_BLOG_URL}/${userId}/${pageNr}`).map(res => res.json()).cache();
    }
    

    blogs:Array<BlogModel>;
    setBlogList(blogs:Array<BlogModel>){
        this.blogs = blogs;
    }
    getBlog(blogId:number, userId?:number):BlogModel{
        if(this.blogs){
            let blog:BlogModel = this.blogs.find((item:BlogModel) => {return item.blogID == blogId});
            return blog;
        }
        //return this.http.get(`${PROFILE_BLOG_URL}/${userId}/1`).map(res => res.json());
        return new BlogModel();

    }

    
    /**
     * @Deprecated
     * 
     * @param userId
     * @returns {Observable<R>}
     */
    getFullProfile(userId:number){

        return this.http.get(`${PROFILE_URL}/${userId}`).map(res => res.json());
    }

    get

}
