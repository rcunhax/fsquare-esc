/**
 * Created by ruic on 18/09/2016.
 */


import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Router, NavigationEnd, Route } from '@angular/router';
import { Observable } from 'rxjs/Observable';

const TOP_ESCORTS_URL = '/api/top-escorts';
const TOP_MOVIES_URL = '/api/top-movies';

@Injectable()
export class ApplicationService {

    navigationHistory:Array<any> = [];
    private flatRoutes:any;

    constructor(private router:Router, private http:Http) {
        this.flatRoutes = {};

        this.buildFlatRoutes(this.router.config);
        console.log('flatRoutes: '+this.flatRoutes);
        this.router.events.subscribe((event) => {
            if(event instanceof NavigationEnd){
                console.log('router.events: '+event);
                this.addNavigationHistory(event);
            }

        });
    }
    buildFlatRoutes(routes:Route[], parent?:Route){
        let parentUrl = '';
        if(parent){
            parentUrl = `${parent.path}/`;
        }
        routes.forEach((route:Route) =>{

            this.flatRoutes[`${parentUrl}${route.path}`] = {
                hasChildren: (route.children && route.children.length > 0),
                parent: parentUrl
            };
            if(route.children && route.children.length > 0){
                this.buildFlatRoutes(route.children, route);
            }
        });
    }

    goBack(){
        this.router.navigateByUrl('');
    }

    addNavigationHistory(navigationEnd:NavigationEnd){

        this.navigationHistory.push({
            name: this.router.config,
            url:navigationEnd.url
        })
    }

    getTopMovies():Observable<any>{
        return this.http.get(`${TOP_MOVIES_URL}?numberOfRecords=4`).map(res => res.json());
    }

    getTopEscorts():Observable<any>{
        return this.http.get(`${TOP_ESCORTS_URL}?numberOfRecords=4`).map(res => res.json());
    }

}