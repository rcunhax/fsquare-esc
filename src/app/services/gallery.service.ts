/**
 * Created by ruic on 07/08/2016.
 */


import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import {GenderModel, CountryModel, RegionModel} from "../models/models";
// import {routes} from "../components/app.routes";

const PROFILE_URL = '/api/profile';
const RATINGS_URL = '/api/profile/ratings';
const GALLERY_URL = '/api/profile/gallery';
const PRIVATE_GALLERY_URL = '/api/profile/private-gallery';
const BLOG_URL = '/api/profile/blog';

@Injectable()
export class GalleryService {
    
    constructor(private http:Http) {
    }

    getRatings(userId:number, pageNr:number):Observable<JSON>{
        return this.http.get(`${RATINGS_URL}/${userId}/${pageNr}`).map(res => res.json());
    }


}
