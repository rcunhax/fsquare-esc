/**
 * Created by ruic on 07/08/2016.
 */


import { Injectable, EventEmitter, AfterContentInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { takeLast } from "rxjs/operator/takeLast";


import { GenderModel, CountryModel, RegionModel, UserProfileResultModel, CountyModel, FilterModel, UserModel } from "../models/models";

//Observable.prototype.takeLast = takeLast;
const COUNTRIES_URL = '/api/countries';
const REGIONS_URL = '/api/regions';
const COUNTIES_URL = '/api/counties';
const PROFILES_URL = '/api/profiles';
const GENDERS_URL = '/api/genders';
const ORIENTATIONS_URL = '/api/orientations';
const PROFILE_URL = '/api/profile';


const MOVIE_FORMATS_URL = '/api/movie-formats';
const DATE_RANGES = ['AllTime', 'LastYear', 'LastMonth', 'LastWeek', 'ThisYear', 'ThisMonth', 'ThisWeek', 'Today', 'Yesterday'];

@Injectable()
export class SearchService{

    country:CountryModel = new CountryModel({
        "countryID": 158,
        "countryName": "United Kingdom",
        "countryISO2": "UK",
        "countryISO3": "GBR",
        "nationality": "British"
    });
    menuPermissionsEvent:EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

    searchResultEmitter:EventEmitter<Array<UserProfileResultModel>> = new EventEmitter<Array<UserProfileResultModel>>();
    onFilterEmitter: EventEmitter<FilterModel> = new EventEmitter<FilterModel>();
    
    onProfileEmitter: EventEmitter<UserModel> = new EventEmitter<UserModel>();

    constructor(private http:Http) {

    }
    
    getProfile(userId:number){
        return this.http.get(`${PROFILE_URL}/${userId}`).map(res => res.json()).subscribe((user:UserModel) => {
            this.onProfileEmitter.emit(user);
        });
    }


    getToFavorites():any{
        let favoritesString = window.localStorage.getItem("favorites");
        let favorites = {};
        if(favoritesString){
            favorites = new CountryModel(JSON.parse(favoritesString));
        }
        return favorites;
    }

    isUserFavorite(userId:number):boolean{
        let favorites = this.getToFavorites();
        return favorites[userId];
    }

    toggleUserFavorite(userId:number):boolean{
        let favorites = this.getToFavorites();
        let isFavorite:boolean = favorites[userId];
        if(isFavorite){
            isFavorite = false;
        }else{
            isFavorite = true;
        }
        favorites[userId] = isFavorite;
        window.localStorage.setItem("favorites", JSON.stringify(favorites));

        return isFavorite;
    }

    setCountry(country:CountryModel){
        this.country = country;
        window.localStorage.setItem("country", JSON.stringify(this.country));
    }

    getCountry():CountryModel{
        console.log('getCountry');
        let country = window.localStorage.getItem("country");
        if(country){
            this.country = new CountryModel(JSON.parse(country));
        }
        return this.country;
    }

    getAllGenders():Observable<Array<GenderModel>>{
        return this.http.get(GENDERS_URL).map(res => res.json());
    }
	getAllOrientations():Observable<Array<GenderModel>>{
        return this.http.get(ORIENTATIONS_URL).map(res => res.json());
    }

    getAllCountries():Observable<Array<CountryModel>>{
        return this.http.get(COUNTRIES_URL).map(res => res.json());
    }

    getCountryRegions():Observable<Array<RegionModel>>{
        return this.http.get(`${REGIONS_URL}/${this.country.countryID}`).map(res => res.json());
    }

    getRegionCounties(region:RegionModel):Observable<Array<CountyModel>>{
        return this.http.get(`${COUNTIES_URL}/${region.regionID}`).map(res => res.json());
    }

    searchProfiles(pageNr:number, data?:any):Observable<Array<UserProfileResultModel>>{
        var queryString;
        if(data){
            var queryArray = [];
            for(let key in data){
                queryArray.push(`${key}=${data[key]}`);
            }
            queryString = `?${queryArray.join('&')}`;
        }
        let url = `${PROFILES_URL}/${this.country.countryID}/${pageNr}${queryString}`;
        console.log('URL: '+url);
        let observable:Observable<Array<UserProfileResultModel>> = this.http.get(url).map(res => res.json());
        observable.subscribe(data => {
            this.searchResultEmitter.emit(data);
        });

        return observable;
    }

}
