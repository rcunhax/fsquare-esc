/**
 * Created by ruic on 17/06/2016.
 */

import {
    async,
    inject,
    TestBed,
    ComponentFixture
} from '@angular/core/testing';

import { BaseRequestOptions, ConnectionBackend, Http } from "@angular/http";
import { MockBackend } from '@angular/http/testing';
import { ActivatedRoute, Router } from '@angular/router';


import { AppComponent } from './app.component';


describe('AppComponent', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            providers: [
                AppComponent,
                Router,
                ActivatedRoute,
                BaseRequestOptions,
                MockBackend,
                {provide: Http,
                    useFactory: (backend, defaultOptions) => {
                        return new Http(backend, defaultOptions);
                    },
                    deps: [MockBackend, BaseRequestOptions]
                },
            ]
        });
    });

    beforeEach(async(() => {
        TestBed.compileComponents();
    }));


    it('should be initialised',
        inject([AppComponent], (app: AppComponent) => {
            expect(app).toBeTruthy();
        })
    );

});