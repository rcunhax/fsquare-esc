/**
 * Created by ruic on 07/07/2016.
 */

import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from "./search/search.component";
import { profileRoutes } from "./profile/profile.routes";
import { HomeComponent } from "./home/home.component";


export const appRoutes: Routes = [
    {path: "", component: HomeComponent},
    {path: "escorts", component: SearchComponent},
    {path: "movies", component: SearchComponent},
    ...profileRoutes,
];

export const routing = RouterModule.forRoot(appRoutes);