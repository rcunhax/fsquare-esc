/**
 * Created by ruic on 01/09/2016.
 */

import { Component, Output, Input, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from "../../services/search.service";
import { GenderModel, OrientationModel, CountryModel, RegionModel, CountyModel, FilterModel } from "../../models/models";

@Component({
    selector: 'fs-filter-display',
    templateUrl: 'app/components/filter-display/filter-display.component.html'
})

export class FilterDisplayComponent{

    @Input() filter:FilterModel = new FilterModel();
    @Input() total:number;
    constructor() {

    }

}
