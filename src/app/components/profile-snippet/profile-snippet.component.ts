import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { RatingsModel, UserModel, UserProfileResultModel } from "../../models/models";
import { SearchService } from "../../services/search.service";
import { ProfileRatesComponent } from "../profile-rates/profile-rates.component";

@Component({
    selector: 'fs-profile-snippet',
    templateUrl: 'app/components/profile-snippet/profile-snippet.component.html'
})

export class ProfileSnippetComponent implements OnInit{
    @Input() user:UserModel;
    @Input() userProfileResult:UserProfileResultModel;
    state:any = {};
    mainImageURL:string = '/images/noimage.svg';
    constructor(private searchService:SearchService){

    }

    getImage(imageName):string{
        return '';
    }

    getProfileImage(){
        if(this.user.images){
            if(this.user.images.hasImage1){
                return this.user.images.image1SquareURL;
            }
            if(this.user.images.hasImage2){
                return this.user.images.image2SquareURL;
            }
            if(this.user.images.hasImage3){
                return this.user.images.image3SquareURL;
            }
        }
    }

    toggleSectionState(section){
        if(this.state[section]){
            this.state[section] = !this.state[section];
        }else{
           this.state[section] = true; 
        }
    }

    closeProfileSnippet(){
        this.user = null;
        this.userProfileResult = null;
        console.log('closeProfileSnippet');
    }

    ngOnInit() {
        //this.setProfileImage();
        // this.searchService.getProfile(this.userProfileResult).subscribe(user:UserModel) => {
        //     this.user = user;
        // });
    }
}