/**
 * Created by ruic on 07/08/2016.
 */

import { Component, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from "../../services/search.service";
import { GenderModel, OrientationModel, CountryModel, RegionModel, CountyModel, FilterModel } from "../../models/models";

@Component({
    selector: 'fs-filter',
    templateUrl: 'app/components/filter/filter.component.html'
})

export class FilterComponent{

    genders:Array<GenderModel> = [];
    orientations:Array<OrientationModel> = [];
    countries:Array<CountryModel> = [];
    regions:Array<RegionModel> = [];
    counties:Array<CountyModel> = [];

    //selectedGenders:Array<number> = [];
    //genderIds:Array<number>;
    //selectedOrientations:Array<number> = [];

    country:CountryModel;

    regionListOpened:boolean;
    countyListOpened:boolean;

    filter:FilterModel = new FilterModel();

    constructor(private searchService:SearchService) {
        this.country = this.searchService.getCountry();
        if(this.country){
            this.searchService.getCountryRegions().subscribe(data => {
                for(let i = 0; i < data.length; i++){
                    this.regions.push(new RegionModel(data[i]));
                }
            });
        }
        
        this.searchService.getAllGenders().subscribe(data => {
            for(let i = 0; i < data.length; i++){
                this.genders.push(new GenderModel(data[i]));
            }
        });

        this.searchService.getAllOrientations().subscribe(data => {
            for(let i = 0; i < data.length; i++){
                this.orientations.push(new OrientationModel(data[i]));
            }
        });

        this.searchService.onFilterEmitter.subscribe((filter:FilterModel) => {
            this.filter = filter;
        });

    }

    toggleItemSelect(item:any){
        if(item['selected']){
            item['selected'] = false;
        }else{
            item['selected'] = true;
        }
    }

    regionSelection(view:boolean){
        this.regionListOpened = view;
    }

    countySelection(view:boolean){
        this.countyListOpened = view;
    }

    toggleRegionItemSelect(region:RegionModel){
        this.filter.county = new CountyModel();
        this.regionListOpened = false;
        this.filter.region = region;
        this.onRegionSelect();
    }

    toggleCountyItemSelect(county:CountyModel){
        this.countyListOpened = false;
        this.filter.county = county;
    }


    onRegionSelect(){
        this.searchService.getRegionCounties(this.filter.region).subscribe(data => {
            this.counties = data;
        })
    }

    applyFilter(){
        this.filter.genders = [];
        this.genders.forEach(item => {
            if(item['selected']){
                this.filter.genders.push(item);
            }
        });

        this.filter.orientations = [];
        this.orientations.forEach(item => {
            if(item['selected']){
                this.filter.orientations.push(item);
            }
        })


        this.searchService.onFilterEmitter.emit(this.filter);
    }
    
}
