import { NgModule, Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { RatingsModel, UserModel, RateBreaksModel } from "../../models/models";
import { SearchService } from "../../services/search.service";
import {ProfileStarRatingsComponent} from "../profile-star-ratings/profile-star-ratings.component";

@Component({
    selector: 'fs-profile-rates',
    templateUrl: 'app/components/profile-rates/profile-rates.component.html'
})

export class ProfileRatesComponent{
    @Input() rateBreaks:Array<RateBreaksModel> = [];
    @Input() title:string;
    constructor(){

    }
}


@NgModule({
    declarations: [
        ProfileStarRatingsComponent,
        ProfileRatesComponent
    ],
    exports: [
        ProfileStarRatingsComponent,
        ProfileRatesComponent
    ],
    imports: [
        CommonModule
    ]
})

export class ProfileRatingsModule {}