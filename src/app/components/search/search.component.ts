/**
 * Created by ruic on 07/08/2016.
 */

import { Component, AfterContentInit, OnDestroy, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { fromEvent } from "rxjs/observable/fromEvent";
import { SearchModel, FilterModel, RegionModel, CountyModel, UserProfileResultModel, UserModel } from "../../models/models";
import { SearchService } from "../../services/search.service";
import { FilterDisplayComponent } from "../filter-display/filter-display.component";
import { ProfileSnippetComponent } from "../profile-snippet/profile-snippet.component";
import { Subscription} from "rxjs/Subscription";
import { ProfileRatingsModule } from "../profile-rates/profile-rates.component";
import { EscortItemModule } from "../escort-item/escort-item.component";
import { PipesModule } from "../../pipes/pipes.module";


@Component({
    selector: 'fs-search',
    templateUrl: 'app/components/search/search.component.html'
})

export class SearchComponent implements AfterContentInit, OnDestroy{

    filter:FilterModel = new FilterModel();
    searchData:any = {};

    profiles:Array<UserProfileResultModel> = [];
    searchModel:SearchModel = new SearchModel({pageNumber:1});
    searchScrollEventSubscription:Subscription;
    listDisplayType:string = 'masonry';
    availableListDisplayTypes:Array<string>  = ['list', 'masonry'];
    userPreview:UserModel;
    isLoading:boolean = false;
    //userProfileResultPreview:UserProfileResultModel;

    orderByValues:Array<any> = [
        {
            label: 'Rating',
            value: 'rating'
        },
        {
            label: 'Register Date',
            value: 'registerdate'
        },
        {
            label: 'Views',
            value: 'views'
        },
        {
            label: 'Last updated',
            value: 'lastupdated'
        }
    ];

    constructor(private searchService:SearchService) {
        
        this.searchService.onProfileEmitter.subscribe((user:UserModel) => {
            this.openProfileSnippet(user);
        });

        this.searchService.searchResultEmitter.subscribe(result => {
            this.setSearchResult(result);
        });

        this.searchService.onFilterEmitter.subscribe((filter:FilterModel) => {
            this.applyFilter(filter);
        });

        if(this.searchService.getCountry()){
            this.search();
        }
    }

    openProfileSnippet(user:UserModel){
        this.userPreview = user;
    }

    ngAfterContentInit(){
        this.searchScrollEventSubscription = fromEvent(window.document.querySelector('.content'), 'scroll').subscribe(ev => {
            this.onScroll(ev);
        })
    }

    goToNextPage(){
        this.search(this.searchModel.getNextPage());
    }

    search(pageNr:number = 1){
        this.isLoading = true;
        this.searchService.searchProfiles(pageNr, this.searchData);
    }

    setSearchResult(result){
        this.isLoading = false;
        this.profiles = this.profiles.concat(result['profiles']);
        this.searchModel = new SearchModel(result);
    }

    hasNoMoreItems():boolean {
        return this.searchModel.pageNumber === this.searchModel.pageCount;
    }
    onScroll(ev){
        let target = ev.currentTarget;
        let safeGap:number = 10;
        if ((target.scrollTop + target.offsetHeight) >= (target.scrollHeight-safeGap)){
            if(this.hasNoMoreItems()) {
                return;
            }
            this.goToNextPage();
        }
    }
    closeCounty(){
        this.filter.county = new CountyModel();
        this.searchService.onFilterEmitter.emit(this.filter);
    }

    closeRegion(){
        this.filter.county = new CountyModel();
        this.filter.region = new RegionModel();
        this.searchService.onFilterEmitter.emit(this.filter);
    }

    changeListDisplayType(){
        if(this.listDisplayType == 'list' ){
            this.listDisplayType = 'masonry';
        }else{
            this.listDisplayType = 'list';
        }
        
    }

    applyFilter(filter?:FilterModel){
        this.searchData = {};
        let selectedOrientations:Array<number> = [];
        let selectedGenders:Array<number> = [];

        if(filter){
            this.filter = filter;
        }

        this.filter.genders.forEach(gender => {
            selectedGenders.push(gender.genderID);
        });
        if(selectedGenders.length > 0) {
            this.searchData['genderIds'] = selectedGenders.join(',');
        }

        this.filter.orientations.forEach(orientation => {
            this.searchData['orientationsIds'] = selectedGenders.join(',');
            selectedOrientations.push(orientation.orientationID);
        });
        if(selectedOrientations.length > 0){
            this.searchData['orientationIds'] = selectedOrientations.join(',');
        }

        if(this.filter.county && this.filter.county.countyID){
            this.searchData['countyId'] = this.filter.county.countyID;
        }
        if(this.filter.region && this.filter.region.regionID) {
            this.searchData['regionId'] = this.filter.region.regionID;
        }

        if(this.filter.region && this.filter.region.regionID) {
            this.searchData['regionId'] = this.filter.region.regionID;
        }

        if(this.filter.isAvailableTodayEscort){
            this.searchData['isAvailableTodayEscort'] = this.filter.isAvailableTodayEscort;
        }
        if(this.filter.isVerified){
            this.searchData['isVerified'] = this.filter.isVerified;
        }
        if(this.filter.hasGallery){
            this.searchData['hasGallery'] = this.filter.hasGallery;
        }

        console.log("this.filter: "+JSON.stringify(this.filter));

        
        this.searchModel = new SearchModel();

        this.search();

    }

    ngOnDestroy(){
       this.searchScrollEventSubscription.unsubscribe();
    }

}

@NgModule({
    declarations: [
        SearchComponent,
        FilterDisplayComponent,
        ProfileSnippetComponent
    ],
    exports: [
        SearchComponent,
        FilterDisplayComponent,
        ProfileSnippetComponent
    ],
    imports : [
        RouterModule,
        ProfileRatingsModule,
        PipesModule,
        CommonModule,
        EscortItemModule
    ]
})

export class SearchModule {}