/**
 * Created by ruic on 07/08/2016.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { UserProfileResultModel, SearchModel, CountryModel, FilterModel, RegionModel, CountyModel } from "../../models/models";
import { SearchService } from "../../services/search.service";
import { ProfileStarRatingsComponent } from "../profile-star-ratings/profile-star-ratings.component";

@Component({
    selector: 'fs-result-profile',
    templateUrl: 'app/components/search/search-result-item.component.html'
})

export class SearchResultItemComponent implements OnInit{
	@Input() profile:UserProfileResultModel;
    
    profileLiked:boolean;
    profileImageLink:string;
    genderIcon:string = 'fa-genderless';
	constructor(private searchService:SearchService){

	}

    openProfileSnippet(){
        this.searchService.getProfile(this.profile.userID);
    }

    toggleLikeProfile(){
        this.profileLiked = this.searchService.toggleUserFavorite(this.profile.userID);
    }

    getProfileImage(){
        if(this.profile.hasProfileThumbnail){
            this.profileImageLink = this.profile.profileThumbnailURL;
        }else{
            if(this.profile.gender){
                this.profileImageLink = `/images/${this.profile.gender.toLowerCase().split(' ').join('_')}.svg`;
            }else{
                this.profileImageLink = `/images/gender_unknown.svg`;
            }
        }
    }
    getProfileGender(){
        if(this.profile.gender){
            if(this.profile.gender === 'Male'){
                this.genderIcon = 'fa-mars';
            }
            if(this.profile.gender === 'Female'){
                this.genderIcon = 'fa-venus';
            }
            if(this.profile.gender === 'Couple MF'){
                this.genderIcon = 'fa-venus-mars';
            }
            if(this.profile.gender === 'Couple MM'){
                this.genderIcon = 'fa-mars-double';
            }
            if(this.profile.gender === 'Couple FF'){
                this.genderIcon = 'fa-venus-double';
            }
        }
    }

    ngOnInit(){
        this.getProfileGender();
        this.getProfileImage();
        this.profileLiked = this.searchService.isUserFavorite(this.profile.userID);
    }
}
