/**
 * Created by ruic on 29/08/2016.
 */


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GalleryComponent } from "../../gallery/gallery.component";
import { ProfileService } from "../../../services/profile.service";
import {FreePictureModel, PictureModel, UserModel } from "../../../models/models";
import { ProfileRatesComponent } from "../../profile-rates/profile-rates.component";
import { ImageSliderComponent } from "../../image-slider/image-slider.component";
import {SearchService} from "../../../services/search.service";


@Component({
    selector: 'fs-profile-details',
    templateUrl: 'app/components/profile/details/details.component.html'
})

export class DetailsComponent implements OnInit{
    userId:number;
    user:UserModel = new UserModel();
    profileLiked:boolean;
    images:Array<any> = [];
    
    sectionState:any = {
        intro: true,
        details: true
    };

    test:string = '';

    constructor(private route: ActivatedRoute, private profileService:ProfileService, private searchService:SearchService) {

    }

    toggleLikeProfile(){
        this.profileLiked = this.searchService.toggleUserFavorite(this.user.profile.userID);
    }


    toggleSection(sectionName:string){
        if(!this.sectionState[sectionName]){
            this.sectionState[sectionName] = true;
        }else{
            this.sectionState[sectionName] = false;
        }

        this.test = JSON.stringify(this.sectionState);
    }

    ngOnInit() {
        this.userId = this.route.snapshot.params['userId'];
        if (this.userId) {
            this.profileService.getProfile(this.userId).subscribe((user:UserModel) => {
                this.user = user;
                this.profileLiked = this.searchService.isUserFavorite(this.user.profile.userID);

                if (this.user.hasImages) {
                    if(this.user.images.hasImage1){
                        this.images.push({
                            src: this.user.images.image1LargeURL,//'/images/beauty-colors-flowers.jpg';
                            name: this.user.images.image1Description
                        })
                    }
                    if(this.user.images.hasImage2){
                        this.images.push({
                            src: this.user.images.image2LargeURL,//'/images/beauty-colors-flowers2.jpg';
                            name: this.user.images.image2Description
                        })
                    }
                    if(this.user.images.hasImage3){
                        this.images.push({
                            src: this.user.images.image3LargeURL,//'/images/beauty-colors-flowers3.jpg';
                            name: this.user.images.image3Description
                        })
                    }

                }
                this.user.details = 'Lorem ipsum dolor sit amet, est vestibulum pharetra lectus ut ornare, vestibulum mi mauris tincidunt quis nisl vestibulum';
            })
        }
    }
}
