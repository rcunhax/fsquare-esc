/**
 * Created by ruic on 04/09/2016.
 */

import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BlogModel, UserModel} from "../../../models/models";
import {ProfileService} from "../../../services/profile.service";


@Component({
    selector: 'fs-blog-detail',
    templateUrl: 'app/components/profile/blog/blog-detail.component.html'
})

export class BlogDetailComponent{
    blog:BlogModel = new BlogModel();
    userId:number;
    constructor(private profileService:ProfileService, private route:ActivatedRoute){
        this.init();
    }

    init(){
        this.userId = this.route.snapshot.params['userId'];
        let blogId = this.route.snapshot.params['blogId'];
        if (blogId) {
            // this.profileService.getBlog(blogId).subscribe(data => {
            //     this.blog = data;
            // });
            this.blog = this.profileService.getBlog(blogId, this.userId);

        }
    }

}