/**
 * Created by ruic on 30/08/2016.
 */

import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogModel, UserModel } from "../../../models/models";
import { ProfileService } from "../../../services/profile.service";


@Component({
    selector: 'fs-profile',
    templateUrl: 'app/components/profile/blog/blog.component.html'
})

export class BlogComponent{
	details:any = {};
    blogs:Array<BlogModel> = [];

    //@Input() user:UserModel;
    constructor(private profileService:ProfileService, private route:ActivatedRoute){
        this.init();
    }

    init(){
        let userId = this.route.snapshot.params['userId'];
        if (userId) {

            this.profileService.getProfile(userId).subscribe((user:UserModel) => {
                this.profileService.getBlogs(user.profile.userID, 1).subscribe(data => {
                    this.details = data;
                    this.blogs = data['blogs'];
                    this.profileService.setBlogList(this.blogs);
                });
            });

        }
    }
}
