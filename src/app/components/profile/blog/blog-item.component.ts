/**
 * Created by ruic on 31/08/2016.
 */

import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BlogModel, UserModel} from "../../../models/models";
import {ProfileService} from "../../../services/profile.service";


@Component({
    selector: 'fs-blog-item',
    templateUrl: 'app/components/profile/blog/blog-item.component.html'
})

export class BlogItemComponent{
    @Input() blog:BlogModel;
    constructor(){


    }

}
