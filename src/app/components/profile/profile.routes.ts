/**
 * Created by ruic on 29/08/2016.
 */


import { Routes } from '@angular/router';
import { DetailsComponent } from "./details/details.component";
import { ProfileComponent } from "./profile.component";
import { PrivateGalleryComponent } from "./private-gallery/private-gallery.component";
import { FreeGalleryComponent } from "./free-gallery/free-gallery.component";
import { BlogComponent } from "./blog/blog.component";
import { FieldReportsComponent } from "./field-reports/field-reports.component";
import { BlogDetailComponent } from "./blog/blog-detail.component";
import { ProfileMoviesComponent } from "./movies/movies.component";

export const profileRoutes: Routes = [
    {path: "profile", component: ProfileComponent, children:
        [
            {path: ":userId/details", component: DetailsComponent },
            {path: ":userId/gallery", component: FreeGalleryComponent },
            {path: ":userId/private-gallery", component: PrivateGalleryComponent },
            {path: ":userId/blog", component: BlogComponent},
            {path: ":userId/blog/:blogId", component: BlogDetailComponent},
            {path: ":userId/field-reports", component: FieldReportsComponent },
            {path: ":userId/movies", component: ProfileMoviesComponent }            
        ]
    }
];
