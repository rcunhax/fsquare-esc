/**
 * Created by ruic on 09/08/2016.
 */

import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { ActivatedRoute, RouterModule } from '@angular/router';
import { UserModel } from "../../models/models";
import { ProfileService } from "../../services/profile.service";
import { PrivateGalleryComponent } from "./private-gallery/private-gallery.component";
import { DetailsComponent } from "./details/details.component";
import { BlogComponent } from "./blog/blog.component";
import { FieldReportComponent } from "./field-reports/field-report.component";
import { FieldReportsComponent } from "./field-reports/field-reports.component";
import { BlogDetailComponent } from "./blog/blog-detail.component";
import { BlogItemComponent } from "./blog/blog-item.component";
import { ImageSliderModule } from "../image-slider/image-slider.component";
import { GalleryModule } from "../gallery/gallery.component";
import { ProfileRatingsModule } from "../profile-rates/profile-rates.component";
import { FreeGalleryComponent } from "./free-gallery/free-gallery.component";
import { ProfileMoviesComponent } from "./movies/movies.component";
import { MovieItemModule } from "../movie-item/movie-item.component";
import { PipesModule } from "../../pipes/pipes.module";
import { ApplicationService } from "../../services/application.service";

@Component({
    selector: 'fs-profile',
    templateUrl: 'app/components/profile/profile.component.html',
    providers: [ProfileService]
})

export class ProfileComponent{
    userId:number;
    user:UserModel = new UserModel({});

    constructor(private route: ActivatedRoute, private profileService: ProfileService, private applicationService:ApplicationService) {
        console.log('ProfileComponent');
        //this.init();
        this.profileService.userSetEvent.subscribe((user:UserModel) => {
            console.log('ProfileComponent userSetEvent');
            this.user = user;
            if(user.profile){
                this.userId = user.profile.userID;
            }
        })
    }

    goBack(){
        this.applicationService.goBack();
        console.log(1);
    }

    init(){
        let userId = this.route.snapshot.params['userId'];
        if (userId) {

            //this.profileService.getRatings()

            this.profileService.getProfile(userId).subscribe((user:UserModel) => {})


        }
    }

}

@NgModule({
    declarations: [
        ProfileComponent,
        PrivateGalleryComponent,
        FreeGalleryComponent,
        ProfileMoviesComponent,
        DetailsComponent,
        BlogComponent,
        BlogDetailComponent,
        BlogItemComponent,
        FieldReportComponent,
        FieldReportsComponent
    ],
    exports: [
        ProfileComponent
    ],
    imports : [
        ProfileRatingsModule,
        ImageSliderModule,
        RouterModule,
        GalleryModule,
        PipesModule,
        CommonModule,
        MovieItemModule
    ]
})

export class ProfileModule {}