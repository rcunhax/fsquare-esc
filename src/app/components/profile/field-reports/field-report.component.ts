/**
 * Created by ruic on 01/09/2016.
 */

import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserModel, FieldReportModel } from "../../../models/models";
import { ProfileService} from "../../../services/profile.service";

@Component({
    selector: 'fs-field-report',
    templateUrl: 'app/components/profile/field-reports/field-report.component.html'
})

export class FieldReportComponent{
    @Input() fieldReport:FieldReportModel;
    constructor(){

    }

}