/**
 * Created by ruic on 01/09/2016.
 */


import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserModel, FieldReportModel } from "../../../models/models";
import { ProfileService} from "../../../services/profile.service";
import { FieldReportComponent } from "./field-report.component";


@Component({
    selector: 'fs-field-reports',
    templateUrl: 'app/components/profile/field-reports/field-reports.component.html'
})

export class FieldReportsComponent{
    fieldReports:Array<FieldReportModel> = [];
    constructor(private profileService:ProfileService, private route:ActivatedRoute){
        this.init();
    }

    init(){
        let userId = this.route.snapshot.params['userId'];

        if (userId) {
            this.profileService.getProfile(userId).subscribe((user:UserModel) => {
                this.profileService.getFieldReport(user.profile.userID, 1).subscribe(data => {
                    this.fieldReports = data['fieldReports'];
                    console.log(this.fieldReports);
                });
            });

        }
    }
}



