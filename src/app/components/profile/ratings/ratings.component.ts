/**
 * Created by ruic on 06/09/2016.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { UserProfileResultModel, SearchModel, CountryModel, FilterModel, RegionModel, CountyModel } from "../../../models/models";

@Component({
    selector: 'fs-profile-ratings',
    templateUrl: 'app/components/profile/ratings/ratings.component.html'
})

export class ProfileRatingsComponent{}