/**
 * Created by ruic on 06/09/2016.
 */

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProfileService } from "../../../services/profile.service";

import { UserModel, MovieModel } from "../../../models/models";

@Component({
    selector: 'fs-profile-movies',
    templateUrl: 'app/components/profile/movies/movies.component.html'
})

export class ProfileMoviesComponent{

    movies:Array<MovieModel> = [];
    user:UserModel;
    currentPage:number = 1;

    constructor(private profileService:ProfileService, private route:ActivatedRoute){
		this.init();
    }

    getMovies(){
        this.profileService.getMovieClips(this.user.profile.userID, this.currentPage).subscribe(data => {
            console.log('concat');
            this.movies.concat(data.movies);
        });
    }

    onScroll(ev){
        let target = ev.currentTarget;
        let safeGap:number = 10;
        if ((target.scrollTop + target.offsetHeight) >= (target.scrollHeight-safeGap)){
            // if(this.hasNoMoreItems()) {
            //     return;
            // }
            this.currentPage++;
            this.getMovies();
        }
    }


    init() {
        let userId = this.route.snapshot.params['userId'];
        if (userId) {
            this.profileService.getProfile(userId).subscribe((user:UserModel) => {
                this.user = user;
                this.getMovies();
            });
        }
    }
}