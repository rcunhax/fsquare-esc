/**
 * Created by ruic on 31/08/2016.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FreePictureModel, UserModel} from "../../../models/models";
import { ProfileService } from "../../../services/profile.service";
import { GalleryComponent } from "../../gallery/gallery.component";


@Component({
    selector: 'fs-private-gallery',
    templateUrl: 'app/components/profile/free-gallery/free-gallery.component.html'
})

export class FreeGalleryComponent{
	freeGalleryImages:Array<FreePictureModel> = [];

    constructor(private profileService:ProfileService, private route:ActivatedRoute){
		this.init();
    }

    init() {

        let userId = this.route.snapshot.params['userId'];

        if (userId) {
            this.profileService.getProfile(userId).subscribe((user:UserModel) => {
                this.profileService.getGallery(user.profile.userID, 1).subscribe(data => {
                    for (let i = 0; i < 13; i++) {
                        if(i%3 == 0){
                            this.freeGalleryImages.push(new FreePictureModel({
                                description: '/images/beauty-colors-flowers3.jpg',
                                thumbUrl: '/images/beauty-colors-flowers3.jpg',
                                previewUrl: '/images/beauty-colors-flowers3.jpg',
                                fullUrl: '/images/beauty-colors-flowers3.jpg',
                                dateAdded: '/images/beauty-colors-flowers3.jpg',
                            }));
                        }else if(i%2 == 0){
                            this.freeGalleryImages.push(new FreePictureModel({
                                description: '/images/beauty-colors-flowers2.jpg',
                                thumbUrl: '/images/beauty-colors-flowers2.jpg',
                                previewUrl: '/images/beauty-colors-flowers2.jpg',
                                fullUrl: '/images/beauty-colors-flowers2.jpg',
                                dateAdded: '/images/beauty-colors-flowers2.jpg'
                            }));
                        }else{
                            this.freeGalleryImages.push(new FreePictureModel({
                                description: '/images/beauty-colors-flowers.jpg',
                                thumbUrl: '/images/beauty-colors-flowers.jpg',
                                previewUrl: '/images/beauty-colors-flowers.jpg',
                                fullUrl: '/images/beauty-colors-flowers.jpg',
                                dateAdded: '/images/beauty-colors-flowers.jpg',
                            }));
                        }

                    }

                });
            });
        }
    }
}
