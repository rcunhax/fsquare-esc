/**
 * Created by ruic on 31/08/2016.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PictureModel, UserModel } from "../../../models/models";
import { ProfileService } from "../../../services/profile.service";
import { GalleryComponent } from "../../gallery/gallery.component";


@Component({
    selector: 'fs-private-gallery',
    templateUrl: 'app/components/profile/private-gallery/private-gallery.component.html'
})

export class PrivateGalleryComponent{
	privateGalleryImages:Array<PictureModel> = [];
    constructor(private profileService:ProfileService, private route:ActivatedRoute){
		this.init();
    }
    
    init() {

        let userId = this.route.snapshot.params['userId'];

        if (userId) {
            this.profileService.getProfile(userId).subscribe((user:UserModel) => {
                this.profileService.getPrivateGallery(user.profile.userID, 1).subscribe(data => {
                    //TODO: logic
                    for (let i = 0; i < 9; i++) {
                        this.privateGalleryImages.push(new PictureModel({
                            fileUrl: '/images/beauty-colors-flowers3.jpg',
                        }));
                    }

                });
            });
        }
    }
}
