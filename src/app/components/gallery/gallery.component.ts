/**
 * Created by ruic on 07/08/2016.
 */

import { NgModule, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';

// import { CanActivate, Router,
//     ActivatedRouteSnapshot,
//     RouterStateSnapshot }    from '@angular/router';

import {RegionModel, CountryModel, GenderModel, CountyModel, SearchModel ,OrientationModel} from "../../models/models";
import {SearchService} from "../../services/search.service";
import {GalleryService} from "../../services/gallery.service";
import {ImageSliderModule} from "../image-slider/image-slider.component";

@Component({
    selector: 'fs-gallery',
    templateUrl: 'app/components/gallery/gallery.component.html'
})

export class GalleryComponent implements OnInit{

    @Input() images:Array<any> = [];

    @Input() titleField: string;
    @Input() descriptionField: string;
    @Input() thumbUrlField: string;
    @Input() previewUrlField: string;
    @Input() fileUrlField: string;
    
    mainImage:any;
    currentIndex:number = -1;
    constructor(private route: ActivatedRoute) {


    }

    resetMainImage(){
        this.mainImage = null;
    }

    hasNext(){
        return (this.currentIndex < (this.images.length - 1));
    }

    hasPrev(){
        return this.currentIndex > 0;
    }

    next(){
        if(this.hasNext()){
            this.mainImage = this.images[this.currentIndex--];
        }
    }

    prev(){
        if(this.hasPrev()){
            this.mainImage = this.images[this.currentIndex--];
        }
    }

    setToMainImage(image:any, index:number){
        this.currentIndex = index;
        this.mainImage = image;
    }

    ngOnInit() {
        Object.assign(this, this.route.data);
    }
    
}


@NgModule({
    declarations: [
        GalleryComponent
    ],
    exports     : [
        GalleryComponent
    ],
    imports     : [
        CommonModule,
        ImageSliderModule
    ]
})

export class GalleryModule {}
