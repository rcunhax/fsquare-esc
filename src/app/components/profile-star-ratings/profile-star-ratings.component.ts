
import { NgModule, Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { fromEvent } from "rxjs/observable/fromEvent";
import { RatingsModel } from "../../models/models";
import { SearchService } from "../../services/search.service";

@Component({
    selector: 'fs-profile-star-ratings',
    templateUrl: 'app/components/profile-star-ratings/profile-star-ratings.component.html'
})

export class ProfileStarRatingsComponent implements OnInit{
    @Input() ratings:RatingsModel;
    stars:Array<number> = [1,2,3,4,5];
    percentage:number = 0;
    starsValue:number = 0;
    constructor(){

    }
    ngOnInit(){
        if(this.ratings){
            //console.log('this.ratings.positive : '+this.ratings.positive );
            let negative:number = Math.abs(this.ratings.negative);
            //console.log('this.ratings.negative: '+negative);
            
            this.percentage = (this.ratings.positive / (this.ratings.positive+negative))*100;
            this.starsValue = this.percentage / 20;
            // let starts = Math.round(this.percentage / 20);
            // for(let i = 0;)
            //console.log((this.ratings.positive / (this.ratings.positive+negative))+' - this.starts: '+(this.starts/20));
        }        
    }
}