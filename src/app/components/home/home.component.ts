/**
 * Created by ruic on 18/09/2016.
 */

import { Component, Directive, HostListener, EventEmitter } from '@angular/core';
import { ApplicationService } from "../../services/application.service";


@Component({
    selector: 'fs-home',
    templateUrl: 'app/components/home/home.component.html'
})
export class HomeComponent {
    images:Array<any> = [
        {
            src: '/images/Gaussian Blurs_027.jpg',
            html:'<a href="#/escorts">Photos</a>'
        },
        {
            src: '/images/Gaussian Blurs_028.jpg',
            name:'',
            html:'<a href="#/movies">Movies</a>'
        }
    ];
    movies:Array<any> = [];
    escorts:Array<any> = [];

    constructor(private applicationService:ApplicationService) {

        this.applicationService.getTopEscorts().subscribe(result => {
            this.escorts = result;
        });

        this.applicationService.getTopMovies().subscribe(result => {
            this.movies = result;
        });

    }
}