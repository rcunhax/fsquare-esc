import { Component, Directive, HostListener, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SearchService } from "../services/search.service";
import { CountryModel } from "../models/models";
import { ApplicationService } from "../services/application.service";


@Directive({
    selector: '[fsSwipeHandle]',
    outputs: ['onSwipeLeft','onSwipeRight']
})
export class SwipeHandleDirective{

    startX:number;
    endX:number;
    onSwipeLeft = new EventEmitter();
    onSwipeRight = new EventEmitter();


    @HostListener('touchstart', ['$event'])
    onTouchStart(event) {
        this.startX = event.touches[0].clientX;
    }
    @HostListener('touchmove', ['$event'])
    onTouchMove(event) {
        this.endX = event.touches[0].clientX;
        console.log('- onTouchMove: '+(this.startX - this.endX));
        let swipeLenght:number = this.startX - this.endX;
        if(swipeLenght < 0){
            //Right
            this.onSwipeRight.emit(swipeLenght);
        }else if(swipeLenght > 0){
            //Left
            this.onSwipeLeft.emit(swipeLenght);
        }
    }
    constructor() {
        console.log('SwipeHandleDirective');
    }
}


@Component({
    selector: 'fs-app',
    templateUrl: 'app/components/app.component.html'
})
export class AppComponent{
    isRightSidebarOpened:boolean;
    isLeftSidebarOpened:boolean;
    isCountrySelectOpened:boolean;
    selectedCountry:CountryModel;
    country:CountryModel;

    countries:Array<CountryModel> = [];

    constructor(private searchService:SearchService, private router:Router, private applicationService:ApplicationService) {
        // this.country = this.searchService.getCountry();
        // if(!this.country){
        //     this.openCountrySelection();
        // }
    }

    setCountry(){
        console.log('setCountry: '+JSON.stringify(this.selectedCountry));
        this.country = this.selectedCountry;
        this.searchService.setCountry(this.country);
        this.isCountrySelectOpened = false
    }

    openCountrySelection(){
        this.isCountrySelectOpened = true;
        this.selectedCountry = this.country;
        if(this.countries.length == 0){
            this.searchService.getAllCountries().subscribe(data => {
                this.countries = data;
            });
        }
    }

}




