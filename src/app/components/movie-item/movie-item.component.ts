import { Component, Input, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieModel } from "../../models/models";
import { RouterModule } from '@angular/router';
import {PipesModule} from "../../pipes/pipes.module";

@Component({
    selector: 'fs-movie-item',
    templateUrl: 'app/components/movie-item/movie-item.component.html'
})

export class MovieItemComponent implements OnInit{

    @Input() movie:MovieModel;
    // duration:string;
    // size:string;
    constructor() {

    }
    // setDuration(){
    //     let hours:number    = Math.floor(this.movie.seconds / 3600);
    //     let minutes:number  = Math.floor((this.movie.seconds - (hours * 3600)) / 60);
    //     let seconds:number  = this.movie.seconds - (hours * 3600) - (minutes * 60);
    //
    //
    //     let hoursDisplay:string;
    //     let minutesDisplay:string;
    //     let secondsDisplay:string;
    //     if (hours < 10) {
    //         hoursDisplay   = "0"+hours;
    //     }
    //     if (minutes < 10) {
    //         minutesDisplay = "0"+minutes;
    //     }
    //     if (seconds < 10) {
    //         secondsDisplay = "0"+seconds;
    //     }
    //     this.duration =  hoursDisplay+':'+minutesDisplay+':'+secondsDisplay;
    // }
    // setSize(decimals?:number){
    //     if(this.movie.size == 0)
    //         return '0 Byte';
    //     var k = 1000;
    //     var dm = decimals + 1 || 3;
    //     var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    //     var i = Math.floor(Math.log(this.movie.size) / Math.log(k));
    //     this.size =  parseFloat((this.movie.size / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    //
    // }
    ngOnInit(){
        // this.setDuration();
        // this.setSize(1);

    }
}

@NgModule({
    declarations: [
        MovieItemComponent
    ],
    exports     : [
        MovieItemComponent
    ],
    imports     : [
        CommonModule,
        RouterModule,
        PipesModule
    ]
})

export class MovieItemModule {}