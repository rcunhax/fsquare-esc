/**
 * Created by ruic on 01/09/2016.
 */


import { Injectable, EventEmitter } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { ProfileService } from "../services/profile.service";
import { Observable } from "rxjs/Observable";


@Injectable()
export class UserGuard implements CanActivate {

    constructor(private profileService:ProfileService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean  {
        
        //this.profileService
        // let isUserLoggedIn = this.authService.isUserLoggedIn();
        // if(!isUserLoggedIn){
        //     window.location.href = '/login';
        // }
        // return isUserLoggedIn;
        return true;
    }
}