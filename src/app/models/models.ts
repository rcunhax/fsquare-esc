/**
 * Created by ruic on 07/08/2016.
 */

export class FilterModel{

    country:CountryModel = new CountryModel();
    county:CountyModel = new CountyModel();
    region:RegionModel = new RegionModel();
    locationZipCode:string;
    locationProximityMiles:number;
    locationProximityKilometres:number;
    genders:Array<GenderModel> = [];
    orientations:Array<OrientationModel> = [];
    isVerified:boolean = false;
    isAvailableTodayEscort:boolean = false;
    hasGallery:boolean = false;
    hasMovieClips:boolean = false;
    hasMovieRentals:boolean = false;
    hasSaleItems:boolean = false;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }

    reset(){
        this.country = null;
        this.county = null;
        this.region = null;
        this.genders = [];
        this.orientations = [];
    }
}

export class CountryModel{

    countryID:number;
    countryName:string;
    countryISO2:string;
    countryISO3:string;
    nationality:string;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class CountyModel{

    countyID:number;
    countyName:string;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }

}

export class OrientationModel{
    orientationID:number;
    orientationName:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class GenderModel{
    genderID:number;
    gender:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class RegionModel{

    regionID:number;
    regionName:string;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }

}

// export class SearchProfileModel{

//     profilesPerPage:number = 10;
//     pageNumber:number = 0;
//     orderBy:string = 'lastupdated';
//     isEscort:boolean = true;
//     isPhoneChat:boolean = true;
//     isSMSChat:boolean = true;
//     isAlternative:boolean = true;
//     isOtherServices:boolean = true;
//     isDirectCam:boolean = true;
//     isDirectCamOnly:boolean = true;
//     isDirectChat:boolean = true;
//     isDirectChatOnly:boolean = true;
//     isVerified:boolean = true;
//     isAvailableTodayEscort:boolean = true;
//     isAvailableNowWebcam:boolean = true;
//     isANWCOrDcaOnline:boolean = true;
//     isAvailableNowPhoneChat:boolean = true;
//     directCamInGroup:boolean = true;
//     directCamFreePreview:boolean = true;
//     directCamCategoryID:boolean = true;
//     hasGallery:boolean = true;
//     hasPrivateGallery:boolean = true;
//     hasMovieClips:boolean = true;
//     hasMovieRentals:boolean = true;
//     hasSaleItems:boolean = true;
//     LocationProximityMiles:number = 10;
//     LocationProximityKilometres:number = 10;
//     LocationZipCode:string;
//     countryID:number;
//     regionID:number;
//     countyID:number;
//     nationalityID:number;
//     ratingsNotHidden:boolean;
//     ratingsPositive:boolean;
//     preferenceIDs:string;
//     genderIDs:string;
//     orientationIds:string;
//     nonExplicitSelfCertified:boolean;
//     groupID:number;
//     TVTS:number;
//     VATIPAddress:string;
//     VATUserID:number;
//     lastUpdated:string;
//     lastUpdatedFrom:string;


//     constructor(data?:any){
//         if(data){
//             Object.assign(this, data);
//         }
//     }
// }
export class SearchModel{

    pageNumber:number = 0;
    pageCount:number;
    profilesPerPage:number;
    profilesTotal:number;
    profiles:Array<UserProfileResultModel>;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }

    getNextPage():number{
        return (this.pageNumber + 1);
    }
}

export class ProfileFAQModel{
    Question:string;
    Answer:string
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class ProfileEnjoyModel{
    enjoyID:number;
    enjoyName:string
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class ProfileEnjoyWithModel{
    genderID:number;
    gender:string
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class UserModel{
	hasProfile:boolean;
	profile:ProfileModel;
	hasDetails:boolean;
	details:string;
	hasImages:boolean;
  	images:ImagesModel;
	hasContact:boolean;
	contact:any;
	hasVitalStats:boolean;
	vitalStats:any;
	hasEnjoys:boolean;
	enjoys:Array<ProfileEnjoyModel>;
	hasEnjoysWith:boolean;
	enjoysWith:Array<ProfileEnjoyWithModel>;
	hasEscortRates:boolean;
	escortRates:any;
	hasFAQs:boolean;
	faQs:Array<ProfileFAQModel>;
	hasInterview:boolean;
	interview:any;
	hasCountry:boolean;
	country:any;
	hasNationality:boolean;
	nationality:any;
	hasFeatured:boolean;
	featured:any;
	contentCounts:any;
	hasContentCounts:boolean;
	hasReturnMotD:boolean;
	returnMotD:any;
	hasLinks:boolean;
	links:any;
	isLoggedInNow:boolean

	constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }

	getUserId():number{
		if(this.profile){
			return this.profile.userID;
		}
		return -1;
	}
}
export class ContentGendersModel{
    genderID:number;
    genderName:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class FieldReportModel{

    reportID:number;
    user:ProfileModel;
    client:FieldReportClientModel;
    meetLocation:string;
    meetDate:string;
    meetDuration:string;
    meetFee:string;
    inCall:boolean;
    outCall:boolean;
    visitAgain:boolean;
    recommend:boolean;
    valueForMoney:string;
    score:number;
    createdDate:string;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class FieldReportClientModel{
    userID:number;
    nickname:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class ContactModel{
	constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class GalleryModel{

    gallerySize: number;
    registrationDuration: number;
    registrationPrice: number;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class PictureModel{
    pictureID: string;
    title: string;
    description: string;
    thumbUrl: string;
    previewUrl: string;
    fileUrl: string;
    width: number;
    height: number;
    fileSize: number;
    nonNude: boolean;
    dateAdded: string;
    albumId: string;
    gallery: GalleryModel;
    user: UserModel;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class FreePictureModel{
    description: string;
    thumbUrl: string;
    previewUrl: string;
    fullUrl: string;
    dateAdded: string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class ImagesModel{
	hasImage1:boolean;
    image1ThumbnailURL:string;
    image1MediumURL:string;
    image1LargeURL:string;
    image1SquareURL:string;
    image1Description:string;
    hasImage2:boolean;
    image2ThumbnailURL:string;
    image2MediumURL:string;
    image2LargeURL:string;
    image2SquareURL:string;
    image2Description:string;
    hasImage3:boolean;
    image3ThumbnailURL:string;
    image3MediumURL:string;
    image3LargeURL:string;
    image3SquareURL:string;
    image3Description:string;
    image1:string;
    image2:string;
    image3:string;

	constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class RateBreaksModel{
    rateBreakID:number;
    rateBreak:string;
    rate:number;
    hours:number;
    isOvernight:boolean;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class ProfileEscortRatesModel{
    CurrencyISO3:string;
    inCalls:Array<RateBreaksModel> = [];
    outCalls:Array<RateBreaksModel> = [];
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class ProfileModel{

	userID:number;
    nickname:string;
    summary:string;
    gender:string;
    istvts:boolean;
    age:number;
    orientation:string;
    countryId:number;
    country:string;
    region:string;
    county:string;
    town:string;
    nationalityId:number;
    nationality:string;
    locationZipCode:string;
    views:number;
    memberSince:string;
    lastLoggedIn:string;
    verified:boolean;
    isEscort:boolean;
    doesOutCalls:boolean;
    doesInCalls:boolean;
    isWebcam:boolean;
    isPhoneChat:boolean;
    issmsChat:boolean;
    isAlternative:boolean;
    isDirectCam:boolean;
    isDirectIM:boolean;
    isDirectChat:boolean;
    availableTodayEscort:boolean;
    availableNowWebcam:boolean;
    availableNowPhoneChat:boolean;
	directCam:DirectCamModel;
	directChat:DirectChatModel;
	hasGallery:boolean;
    hasPrivateGallery:boolean;
    hasMovieClips:boolean;
    hasMovieRentals:boolean;
    hasSaleItems:boolean;
    ratings:RatingsModel;
	isCouple:boolean;
    inGroup:boolean;
    groupID: number;
    groupName: string;
    profileURL:string;
    ratingsURL:string;
    hasBlog:boolean;
    lastUpdated:Date;

	constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class MovieModel{

    movieID:string;
    title:string;
    details:string;
    thumbURL:string;
    imageURL:string;
    credits:number;
    seconds:number;
    size:number;
    format:string;
    categories:Array<MovieCategoryModel>;
    dateUploaded:Date;
    gendersInClip: ContentGendersModel;
    user:UserModel;
    
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class MovieCategoryModel{
    categoryID:number;
    categoryName:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class BlogModel{
    blogID:number;
    blogDate:number;
    createDate:string;
    lastUpdated:string;
    subject:string;
    body:string;
    hasImage:boolean;
    thumbURL:string;
    imageURL:string;
    fullURL:string;
    user:UserModel;

    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}

export class UserProfileResultModel extends ProfileModel{
    locationProximityMiles:number;
    locationProximityKilometres:number;
    hasProfileThumbnail:boolean;
    profileThumbnailURL:string;


    constructor(data?:any){
        super(data);
        if(data){
            Object.assign(this, data);
        }
    }
}


export class RatingsModel{
	total:number;
	positive:number;
	neutral:number;
	negative:number;
	ratings:number;
	disputes:number;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class DirectChatModel{
	PPM:string;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
export class DirectCamModel{
	modeID:string;
	mode:string;
	modeColour:string;
	chatOnly:string;
	doesFreePreview:boolean;
	doesFreeToGuest: boolean;
	doesGroup:boolean;
	groupPPM:string;
	doesPrivate:boolean;
	privatePPM:string;
	directCamLink:string;
	allowVoyeur:boolean;
	tipsOnly:boolean;
	hasSpecialOffers:boolean;
    constructor(data?:any){
        if(data){
            Object.assign(this, data);
        }
    }
}
